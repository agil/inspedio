package com.inspedio.system.helper.payment;

import com.inspedio.entity.primitive.InsCallback;
import com.tqm.tqp.VirtualGoodDetails;


/**
 * This class is used for handling payment request
 */
public class PaymentHandler {

	/**
	 * This must be called before any payment can be made.
	 * You can call this after splash screen.
	 */
	public static void init(){
		InsPaymentTequila.init();
	}
	
	/**
	 * Call this to get List of Virtual Goods (you have to call init() before).
	 * You can get Price, Currency, and Amount from Virtual Good getter method. 
	 */
	public static VirtualGoodDetails[] getVirtualGoods(){
		return InsPaymentTequila.getVirtualGoods();
	}
	
	/**
	 * Use this method to request payment.
	 * Success Callback will be called if payment succes, else Failed Callback will be called
	 */
	public static void requestPayment(VirtualGoodDetails vg, InsCallback successCallback, InsCallback failedCallback){
		InsPaymentTequila.requestPayment(vg, successCallback, failedCallback);
	}
}
